export class SnapShare{

    constructor(public title: string,
        public description: string,
        public imageUrl: string,
        public createdDate: Date,
        public likes: number) {
    }

}