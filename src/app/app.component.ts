import { Component, OnInit } from '@angular/core';
import { SnapShare } from './models/face-share.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  mySnap!: SnapShare;

  ngOnInit(): void {
    this.mySnap = new SnapShare(
      'Bugatti "La Voiture Noire"',
      'Elle est sobrement baptisée "La Voiture Noire". Un patronyme qui souligne son caractère unique. Tout juste dévoilée par Bugatti au salon automobile de Genève (Suisse), "La Voiture Noire" est un exemplaire unique. C’est aussi la voiture neuve la plus chère au monde: 11 millions d’euros hors taxe, soit 16,7 millions d\'euros pour son propriétaire, qui n\'est autre que Ferdinand Piëch, ancien dirigeant du Groupe Volkswagen',
      'https://images.bfmtv.com/UsUszd-6qH5LSvmGP4LK5ZkJgwE=/4x3:1252x705/800x0/images/-180591.jpg',
      new Date(),
      0
    );
  }

}
