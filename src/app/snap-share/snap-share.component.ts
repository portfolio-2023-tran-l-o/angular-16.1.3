import { Component, OnInit, Input } from '@angular/core';
import { SnapShare } from '../models/face-share.model';

@Component({
  selector: 'app-snap-share',
  templateUrl: './snap-share.component.html',
  styleUrls: ['./snap-share.component.scss']
})
export class SnapShareComponent implements OnInit {
  @Input() snapShare!: SnapShare;

  buttonTxt !: string;

  ngOnInit(): void{
    this.buttonTxt = 'Like';
  }

  onSnap() {
    if (this.buttonTxt == 'Like' ) {
      this.snapShare.likes++;
      this.buttonTxt = 'Unlike';
    } else {
      this.snapShare.likes--;
      this.buttonTxt = 'Like';
    }
  }

}
