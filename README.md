# Pré-requis, (Le CLI Angular 16.1.X nécessite une version ^16.14.0 || ^18.10.0 de NodeJS)
Pour installer une version précise de Node.js sur notre machine, nous pouvons utiliser l'utilitaire Node Version Manager (NVM). Voici les étapes pour installer une version spécifique de Node.js :
```
    sudo apt install curl
```
Cette commande en dessous télécharge et exécute le script d'installation de NVM.
```
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
```


Une fois l'installation de NVM terminée, fermez et rouvrez le terminal pour charger les modifications de votre profil.
Vérifiez que NVM est installé en exécutant la commande suivante :
```
nvm --version
nvm install --lts
```
![nvminstall.jpg](doc/nvminstall.jpg)

# PROJET EN REMOTE
Pour ma part je le fais en remote sur VSCode, mon projet est stocké dans une machine virtuelle , donc il faut installer l'extension Remote-SSH
et sur la machine virtuelle il faudra installer le package OpenSSH Server
```
    sudo apt install openssh-server
    sudo service ssh status
```
On vérifie que le service est bien actif 

![statusssh.jpg](doc/statusssh.jpg)

Maintenant, nous devrions être en mesure de nous connecter à notre machine virtuelle Ubuntu via Remote-SSH de VSCode.

ssh <nom_utilisateur>@<adresse_IP_Machine>

![ssh.jpg](doc/ssh.jpg)

Puis une fois connecté, nous sommes connectés au terminal de la machine virtuelle  

![home.jpg](doc/home.jpg)

# Angular CLI 16.1.3

![nginstallCLI.jpg](doc/nginstallCLI.jpg)

Voici la documentation du [CLI d'Angular](https://angular.io/cli)  
```
    ng new (nom_du_projet) --style=scss --(options)
```
![app.jpg](doc/app.jpg)

![generatecomponent.jpg](doc/generatecomponent.jpg)


![newcomponentfolder.jpg](doc/newcomponentfolder.jpg)















# SnapShare

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
